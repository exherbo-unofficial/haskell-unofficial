# Copyright 2012 Ivan Miljenovic
# Distributed under the terms of the GNU General Public License v2

require hackage [ ghc_dep='>=6.10' ]

SUMMARY="Library for command line parsing and online help"
DESCRIPTION="
A commandline parsing library, based on getopt. Comes with a powerful attribute system. Supports
complex interfaces with many options and commands, with option & command grouping, with simple and
convenient API. Even though quite powerful, it strives to keep simple things simple. The library
uses System.Console.GetOpt as its backend.

In comparison to the other commandline handling libraries:

Compared to cmdargs, cmdlib has a pure attribute system and is based on GetOpt for help formatting &
argument parsing. Cmdlib may also be more extendable due to typeclass design, and can use
user-supplied types for option arguments.

Cmdargs >= 0.4 can optionally use a pure attribute system, although this is clearly an add-on and
the API is a second-class citizen in relation to the impure version.

GetOpt and parseargs both require explicit flag representation, so they live a level below cmdlib.
GetOpt is in fact used as a backend by cmdlib.
"

LICENCES="BSD-3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/mtl[>=2]
        dev-haskell/transformers[>=0.2.2]
        dev-haskell/split
        dev-haskell/syb
    ")
"

