# Copyright 2010, 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Manipulating Haskell source: abstract syntax, lexer, parser, and pretty-printer"
DESCRIPTION="
Haskell-Source with Extensions (HSE, haskell-src-exts) is an extension
of the standard haskell-src package, and handles most registered
syntactic extensions to Haskell, including:

* Multi-parameter type classes with functional dependencies

* Indexed type families (including associated types)

* Empty data declarations

* GADTs

* Implicit parameters

* Template Haskell

and a few more. All extensions implemented in GHC are supported. Apart
from these standard extensions, it also handles regular patterns as
per the HaRP extension as well as HSX-style embedded XML syntax.
"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-haskell/happy[>=1.17]
    $(haskell_lib_dependencies "
        dev-haskell/array[>=0.1]
        dev-haskell/cpphs[>=1.3]
        dev-haskell/pretty[>=1.0]
    ")
    $(haskell_test_dependencies "
        dev-haskell/containers
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/mtl
        dev-haskell/pretty-show
        dev-haskell/smallcheck[>=1.0]
        dev-haskell/syb
        dev-haskell/tasty[>=0.3]
        dev-haskell/tasty-golden[>=2.2.2]
        dev-haskell/tasty-smallcheck
    ")
"

# Missing example tests/examples/HaddockComments.hs.comments.golden
RESTRICT=test

