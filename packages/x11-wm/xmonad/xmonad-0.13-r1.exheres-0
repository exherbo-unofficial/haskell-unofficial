# Copyright 2008 Santiago M. Mola
# Copyright 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xmonad-0.7.ebuild', which is:
#   Copyright 1999-2008 Gentoo Foundation

require hackage [ has_bin=true ]

SUMMARY="A lightweight X11 window manager"
DESCRIPTION="
Xmonad is a tiling window manager for X. Windows are arranged automatically to tile the screen
without gaps or overlap, maximising screen use. All features of the window manager are accessible
from the keyboard: a mouse is strictly optional. xmonad is written and extensible in Haskell. Custom
layout algorithms, and other extensions, may be written by the user in config files.  Layouts are
applied dynamically, and different layouts may be used on each workspace. Xinerama is fully
supported, allowing windows to be tiled on several screens.
"
HOMEPAGE="http://www.xmonad.org/"

LICENCES="BSD-3"
SLOT="0" # XXX
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""
RESTRICT="test"

DEPENDENCIES="
    build+run:
        x11-libs/libX11
        x11-libs/libXinerama
        x11-libs/libXrandr

    $(haskell_lib_dependencies "
        dev-haskell/containers
        dev-haskell/data-default
        dev-haskell/directory
        dev-haskell/extensible-exceptions
        dev-haskell/filepath
        dev-haskell/mtl
        dev-haskell/process
        dev-haskell/setlocale
        dev-haskell/unix
        dev-haskell/utf8-string[>=0.3&<1.1]
        dev-haskell/X11[>=1.8&<1.9]
    ")

    $(haskell_bin_dependencies "
        dev-haskell/mtl
        dev-haskell/unix
        dev-haskell/X11
    ")

    $(haskell_test_dependencies "
        dev-haskell/containers
        dev-haskell/extensible-exceptions
        dev-haskell/QuickCheck[>=2]
        dev-haskell/X11
    ")
"

SAMPLE_CONFIG="xmonad.hs"
SAMPLE_CONFIG_LOC="man"

src_install() {
    cabal_src_install

    echo -e "#!/bin/sh\n/usr/bin/xmonad" > "${TEMP}/${PN}"
    exeinto /etc/X11/Sessions
    doexe "${TEMP}/${PN}"

    insinto /usr/share/xsessions
    doins "${FILES}/${PN}.desktop"

    doman man/${PN}.1

    dodoc CONFIG README.md "${SAMPLE_CONFIG_LOC}/${SAMPLE_CONFIG}"
}

pkg_postinst() {
    haskell_pkg_postinst

    elog "A sample ${SAMPLE_CONFIG} configuration file can be found here:"
    elog "    /usr/share/doc/${PNVR}/${SAMPLE_CONFIG}"
    elog "The parameters in this file are the defaults used by xmonad."
    elog "To customize xmonad, copy this file to:"
    elog "    ~/.xmonad/${SAMPLE_CONFIG}"
    elog "After editing, use 'mod-q' to dynamically restart xmonad "
    elog "(where the 'mod' key defaults to 'Alt')."
    elog ""
    elog "Read the README or man page for more information, and to see "
    elog "other possible configurations go to:"
    elog "    http://haskell.org/haskellwiki/Xmonad/Config_archive"
    elog "Please note that many of these configurations will require the "
    elog "x11-plugins/xmonad-contrib package to be installed."
}

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/89a8cc88c31163dd35d3182bbba7cdd0630ff54b.patch )

